
HDF5_DIR = /Users/mowkes/Repos/opt/hdf5-1.10.1
SZIP_DIR = /Users/mowkes/Repos/opt/szip-2.1.1
SILO_DIR = /Users/mowkes/Repos/opt/silo-4.10.2

SILO_INC = -I$(SILO_DIR)/include
SILO_LIB = -L$(SILO_DIR)/lib -lsiloh5 -L$(HDF5_DIR)/lib -lhdf5 -L$(SZIP_DIR)/lib -lsz -lz -lstdc++ #-L/usr/local/lib -lstdc++ -lz

C_SRC := $(wildcard *.c)
F_SRC := $(wildcard *.f90)

F90 := mpifort #gfortran
CC  := mpicc   #gcc

FFLAGS := -g -fbounds-check -Wall -pedantic -fbacktrace

OBJECTS := $(C_SRC:%.c=%.o) $(F_SRC:%.f90=%.o)

run: silo_writeRead
	        mpirun -np 4 ./silo_test

silo_writeRead: $(OBJECTS)
	        $(F90) -o silo_test *.o -lc $(SILO_LIB)

%.o: %.c
	        $(CC) $(SILO_INC) -c $< -o $@

%.o: %.f90
	        $(F90) $(FFLAGS) $(SILO_INC) -c $< -o $@

clean:
	        rm -fr *.o silo_test Visit
