program silo_test
  use mpi
  implicit none
  include "silo_f9x.inc"
  ! Precision
  integer, parameter :: SP = kind(1.0)
  integer, parameter :: DP = kind(1.0d0)
  integer, parameter :: WP = DP

  ! Time info
  integer :: n
  integer, parameter :: Nstep=100
  real(WP) :: time, dt=0.1_WP

  ! Parallel info
  integer :: comm,comm_2D
  integer :: irank,iroot,nproc,ierr
  integer :: irankx,iranky,nprocx,nprocy
  integer :: imin ,imax ,jmin ,jmax  ! Global grid
  integer :: imin_,imax_,jmin_,jmax_ ! Local grid
  integer :: nx_,ny_

  ! Data
  integer, parameter :: nx=100, ny=50
  real(WP), dimension(nx+1) :: x  ! Cell face
  real(WP), dimension(ny+1) :: y
  real(WP), dimension(nx  ) :: xm ! Cell center
  real(WP), dimension(ny  ) :: ym
  real(WP), dimension(:,:), allocatable :: T
  real(WP), parameter :: Lx=5.0_WP
  real(WP), parameter :: Ly=2.5_WP

  ! Initalization
  ! -----------------------
  time=0.0_WP
  call parallel_init
  call grid_init
  call data_init
  call silo_init

  ! Time loop
  ! ----------------------
  do n=1,Nstep
     if (irank.eq.iroot) print *,'Step ',n,'/',Nstep,' Time=',time

     ! Create data at this time
     call data_update
     
     ! Write data to silo
     call silo_writeData

     ! Update time
     time=time+dt
  end do
  
  ! Done
  ! ----------------------
  if (irank.eq.iroot) print *,'Program Complete'
  call MPI_Finalize (ierr)

contains
  ! ======================== !
  ! Initialize silo database !
  ! ======================== !
  subroutine silo_init
    implicit none

    ! Make Visit directory
    if (irank.eq.iroot) call execute_command_line('mkdir -p Visit')

    return
  end subroutine silo_init

  ! ================== !
  ! Write data to silo !
  ! ================== !
  subroutine silo_writeData
    implicit none
    integer :: dbfile
    character(len=50) :: dirname,siloname
    character(len=10), dimension(nproc):: names
    integer, dimension(nproc) :: lnames,types

    integer :: currentProc

    ! Create the silo database
    ! ------------------------
    write(siloname,'(A,I0.5,A)') 'Visit/step',n,'.silo'
    if (irank.eq.iroot) then
       ierr = dbcreate(siloname, len_trim(siloname), DB_CLOBBER, DB_LOCAL,"Silo database", 13, DB_HDF5, dbfile)
       if(dbfile.eq.-1) print *,'Could not create Silo file!'
       ierr = dbclose(dbfile)
    end if
    call MPI_BARRIER(MPI_COMM_WORLD,ierr)


    ! Each Proc writes their data
    ! ---------------------------
    
    ! Loop over procs and each proc writes their portion of the mesh and data
    do currentProc=0,nproc
       if (irank.eq.currentProc) then ! This proc gets to write
          
          ! Open silo file 
          ierr = dbopen(siloname,len_trim(siloname),DB_HDF5,DB_APPEND,dbfile)

          ! Create directory inside silo file for this proc
          write(dirname,'(I5.5)') irank
          ierr = dbmkdir(dbfile,dirname,5,ierr)
          ierr = dbsetdir(dbfile,dirname,5)
          
          ! Write quad-mesh for this proc
          ierr = dbputqm(dbfile,"Mesh",4,'xc',2,'yc',2,"zc",2, &
             real(x(imin_:imax_+1),SP),real(y(jmin_:jmax_+1),SP),DB_F77NULL, &
             (/nx_+1,ny_+1/),2,DB_FLOAT,DB_COLLINEAR,DB_F77NULL,ierr)

          ! Write data for this proc
          ierr = dbputqv1(dbfile, 'Temp',4, &
               "Mesh", 4, real(T(imin_:imax_,jmin_:jmax_),SP), &
               (/nx_,ny_/), 2, DB_F77NULL, 0, DB_FLOAT, DB_ZONECENT,DB_F77NULL, ierr)
          
          ! Close silo file
          ierr = dbclose(dbfile)
          
       end if
       ! Everyone waits until this proc is done
       call MPI_BARRIER(MPI_COMM_WORLD,ierr)
    end do

    ! Write Multimesh/Multivars
    ! -------------------------
    ! (tells visit where to find all the parts of the mesh/variables)
    if (irank.eq.iroot) then
       ! Open silo file
       ierr = dbopen(siloname,len_trim(siloname),DB_HDF5,DB_APPEND,dbfile)
       
       ! Increase length of string in silo
       ierr = dbset2dstrlen(10)

       ! Create names to the parts of the mesh
       do currentProc=1,nproc
          write(names(currentProc),'(I5.5,A)') currentProc-1,'/Mesh'
          lnames(currentProc)=len_trim(names(currentProc))
          types(currentProc)=DB_QUAD_RECT
       end do
       ! Write multimesh
       ierr = dbputmmesh(dbfile,  "Mesh", 4, nproc, names,lnames, types, DB_F77NULL, ierr)

       ! Create names to the parts of the quadvars
       do currentProc=1,nproc
          write(names(currentProc),'(I5.5,A)') currentProc-1,'/Temp'
          lnames(currentProc)=len_trim(names(currentProc))
          types(currentProc)=DB_QUADVAR
       end do
       ! Write multimesh
       ierr = dbputmvar(dbfile,  "Temp", 4, nproc, names,lnames, types, DB_F77NULL, ierr)

       ! Close the silo file
       ierr = dbclose(dbfile)

    end if


    return
  end subroutine silo_writeData

  ! =============================== !
  ! Initialize parallel environment !
  ! =============================== !
  subroutine parallel_init
    implicit none
    integer :: rem
    integer, dimension(2) :: dims
    logical, dimension(2) :: peri
    integer, dimension(2) :: coords

    ! Communicator
    call MPI_INIT(ierr)
    ! Duplicate communicator
    call mpi_comm_dup(mpi_comm_world,comm,ierr)
    ! Get ranks
    call MPI_COMM_RANK(comm,irank,ierr)
    call MPI_COMM_SIZE(comm,nproc,ierr)

    ! Split procs into nprocx and nprocy - evenly
    nprocx=int(sqrt(real(nproc,WP)))
    nprocy=int(sqrt(real(nproc,WP)))
    if (nprocx*nprocy.ne.nproc) then
       print *,'# procs must by square number'
       call MPI_FINALIZE(ierr)
       stop
    end if

    ! Cartesian grid of procs
    dims=(/nprocx,nprocy/)
    peri=.false.
    call MPI_CART_CREATE(comm,2,dims,peri,.false.,comm_2D,ierr)
    call MPI_CART_COORDS(comm_2D,irank,2,coords,ierr)
    irankx=coords(1)
    iranky=coords(2)

    ! Root processor
    iroot=0

    ! Global index
    imin=1; imax=nx
    jmin=1; jmax=ny

    ! Distribute grid amongst procs
    nx_=nx/nprocx
    rem=mod(nx,nprocx)
    if (irankx.lt.rem) nx_=nx_+1
    imin_=imin+irankx*(nx/nprocx)+min(irankx,rem)
    imax_=imin_+nx_-1
    ! y direction
    ny_=ny/nprocy
    rem=mod(ny,nprocy)
    if (iranky.lt.rem) ny_=ny_+1
    jmin_=jmin+iranky*(ny/nprocy)+min(iranky,rem)
    jmax_=jmin_+ny_-1

    return
  end subroutine parallel_init


  ! ======================= !
  ! Arbitrary data array    !
  ! Zone centered           !
  ! ======================= !
  subroutine data_init
    implicit none
    
    ! Allocate temp array
    allocate(T(imin_:imax_,jmin_:jmax_))

    return 
  end subroutine data_init

  subroutine data_update
    implicit none
    integer :: i,j
    real(WP), parameter :: pi=3.141592653589793238_WP

    do j=jmin_,jmax_
       do i=imin_,imax_
          T(i,j)=sin(2.0_WP*pi*(xm(i)-time))*sin(2.0_WP*pi*ym(j))
       end do
    end do

    return
  end subroutine data_update

  ! =============== !
  ! Initialize grid !
  ! =============== !
  subroutine grid_init
    implicit none
    integer :: i,j
    
    ! Create grid
    do i=1,nx+1
       x(i)=real(i-1,WP)/real(nx,WP)*Lx
    end do
    do j=1,ny+1
       y(j)=real(j-1,WP)/real(ny,WP)*Ly
    end do
    xm=0.5_WP*(x(2:nx+1)+x(1:nx))
    ym=0.5_WP*(y(2:ny+1)+y(1:ny))
  end subroutine grid_init
  

end program




